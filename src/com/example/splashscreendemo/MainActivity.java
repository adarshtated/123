package com.example.splashscreendemo;

import android.location.GpsStatus.NmeaListener;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

@SuppressLint("ShowToast")
public class MainActivity extends Activity {

	RadioButton rDentist,rOther,rCounOther,rIndia, rPracticeYes,rPracticeNo,rWebYes,rWebNo;
	EditText edtOther,edtCounOther,edtNm,edtMail;
	RadioGroup rg,rgc,rgp,rgw;
	
	//String profession;
	
	DbHelperDentistDetail dbHelperDentistDetail;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.dentist_detail);
		//rOther=(RadioButton)findViewById(R.id.radioOther);
		edtOther=(EditText)findViewById(R.id.edtProfession);
		edtCounOther=(EditText)findViewById(R.id.edtCountry);
		edtNm=(EditText)findViewById(R.id.edtName);
		edtMail=(EditText)findViewById(R.id.edtEmail);
		
		rDentist=(RadioButton)findViewById(R.id.radioDentist);
		rOther=(RadioButton)findViewById(R.id.radioOther);
		rCounOther=(RadioButton)findViewById(R.id.radioCounOther);
		rIndia=(RadioButton)findViewById(R.id.radioIndia);
		rPracticeYes=(RadioButton)findViewById(R.id.radioPracYes);
		rPracticeNo=(RadioButton)findViewById(R.id.radioPracNo);
		rWebYes=(RadioButton)findViewById(R.id.radioWYes);
		rWebNo=(RadioButton)findViewById(R.id.radioWNo);
		
		rg =(RadioGroup)findViewById(R.id.radioProfes);
		rgc=(RadioGroup)findViewById(R.id.radioGrpCountry);
		rgp=(RadioGroup)findViewById(R.id.radioGrpdenprac);
		rgw=(RadioGroup)findViewById(R.id.radiogrpWeb);
		

		

		//dbHelperDentistDetail.addDentalDetail(new DentalDetail(name, email, profession, country, webDentist, dentalPractice));
		

	}
	
	public String onCheckProfession()
	{
		String profession = null;
		int selectedId = rg.getCheckedRadioButtonId();

		if(selectedId != -1) {    
		   // find the radiobutton by returned id 
		   RadioButton selectedRadioButton = (RadioButton) findViewById(selectedId);

		   if(selectedRadioButton==rDentist)
		   {
		   // do what you want with radioButtonText (save it to database in your case)
		profession = (String) selectedRadioButton.getText();
		   
		//   Toast.makeText(getApplicationContext(), "sf"+radioButtonText, 1000).show();
		}
		   else
		   {
			  profession=edtOther.getText().toString();   
			 //  Toast.makeText(getApplicationContext(), "sf"+profession, 1000).show();
 
		   }
		}
		return profession;
	}
	
	public String onCheckCountry()
	{
		String country = null;
		int selectedId = rgc.getCheckedRadioButtonId();

		if(selectedId != -1) {    
		   // find the radiobutton by returned id
		   RadioButton selectedRadioButton = (RadioButton) findViewById(selectedId);

		   if(selectedRadioButton==rIndia)
		   {
		   // do what you want with radioButtonText (save it to database in your case)
		country = (String) selectedRadioButton.getText();
		   
		//   Toast.makeText(getApplicationContext(), "sf"+radioButtonText, 1000).show();
		}
		   else
		   {
			  country=edtCounOther.getText().toString();   
			 //  Toast.makeText(getApplicationContext(), "sf"+profession, 1000).show();
 
		   }
		}
		return country;
	}
	
	public String onCheckDentalPrac()
	{
		String country = null;
		int selectedId = rgp.getCheckedRadioButtonId();

		if(selectedId != -1) {    
		   // find the radiobutton by returned id
		   RadioButton selectedRadioButton = (RadioButton) findViewById(selectedId);

		 
		   // do what you want with radioButtonText (save it to database in your case)
		country = (String) selectedRadioButton.getText();
		   
		//   Toast.makeText(getApplicationContext(), "sf"+radioButtonText, 1000).show();
		
		}
		return country;
	}
	
	
	public String onCheckWeb()
	{
		String yes = null;
		int selectedId = rgw.getCheckedRadioButtonId();

		if(selectedId != -1) {    
		   // find the radiobutton by returned id
		   RadioButton selectedRadioButton = (RadioButton) findViewById(selectedId);

		 
		   // do what you want with radioButtonText (save it to database in your case)
		yes = (String) selectedRadioButton.getText();
		   
		//   Toast.makeText(getApplicationContext(), "sf"+radioButtonText, 1000).show();
		
		}
		return yes;
	}
	
	
	public void onRadioButtonClick(View v)
	{
		boolean checked = ((RadioButton)v).isChecked();
		
		switch (v.getId()) {
		case R.id.radioOther:
			if(checked)
			{
				edtOther.setVisibility(View.VISIBLE);
			}
			else
			{
				edtOther.setVisibility(View.GONE);
			}
			break;
			
		case R.id.radioDentist:
			if(checked)
			{
				edtOther.setVisibility(View.GONE);
			}
			break;
				
		case R.id.radioIndia:
				if(checked)
				{
					edtCounOther.setVisibility(View.GONE);
				}
				break;	
				
		case R.id.radioCounOther:
			if(checked)
			{
				edtCounOther.setVisibility(View.VISIBLE);
			}
			else
			{
				edtCounOther.setVisibility(View.GONE);
			}
			break;		
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	
	}
	
	@SuppressLint("ShowToast")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) 
		{
		case R.id.menu_save:
			
			DentalFunction dentalFunction = new DentalFunction(this);
			DentalDetail detail = new DentalDetail();
			
			String name=edtNm.getText().toString();
			String email=edtMail.getText().toString();
			
			String otherprof=onCheckProfession();
			
			String otherCoun=onCheckCountry();
			
			String webDentist=onCheckWeb();
			String practice=onCheckDentalPrac();
		
			detail.setName(name);
			detail.setEmail(email);
			detail.setProfession(otherprof);
			detail.setCountry(otherCoun);
			detail.setDentalPractice(practice);
			detail.setWebDentist(webDentist);
			
			 long flag= dentalFunction.addDetail(detail);
		       if(flag!=0)
		       {
		    	   Toast.makeText(getApplicationContext(),"Record Added", Toast.LENGTH_LONG).show();		    	   
		       }			
			
			//Toast.makeText(getApplicationContext(), "sf"+name+" "+email+" "+otherprof+" "+otherCoun+" "+webDentist+" "+practice, 1000).show();
			
			//dbHelperDentistDetail.addDentalDetail(new DentalDetail(name, email, otherprof, otherCoun, webDentist, practice));
			 Log.d("Insert: ", "Inserting ..");
			 //Toast.makeText(getApplicationContext(), "Inserterte", 1000).show();
			 break;

		}
		
		return super.onOptionsItemSelected(item);
	}

}
