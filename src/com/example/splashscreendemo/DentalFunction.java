package com.example.splashscreendemo;



import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DentalFunction {

	Context context;
	public DentalFunction(Context context) {
	this.context= context;
	}

	public long addDetail(DentalDetail dentalDetail)
	{   
		
		DbHelperDentistDetail dbHelper = new DbHelperDentistDetail(context);
		SQLiteDatabase db=dbHelper.getWritableDatabase();
		ContentValues cv = new ContentValues();
		//cv.put(DbHelperDentistDetail.KEY_ID, dentalDetail.getId());

		cv.put(DbHelperDentistDetail.KEY_NAME, dentalDetail.getName());
		
		cv.put(DbHelperDentistDetail.KEY_EMAIL, dentalDetail.getEmail());

		cv.put(DbHelperDentistDetail.KEY_PROFESSION, dentalDetail.getProfession());
		
		cv.put(DbHelperDentistDetail.KEY_COUNTRY, dentalDetail.getCountry());
		
		cv.put(DbHelperDentistDetail.KEY_WEB_DENTIST, dentalDetail.getWebDentist());

		cv.put(DbHelperDentistDetail.KEY_DENTAL_PRACTICE, dentalDetail.getDentalPractice());
	
		long i=db.insert(DbHelperDentistDetail.TABLE_NAME,null, cv);
		Log.e("Inserted values", i+"");
		return i;
		
	}
}
