package com.example.splashscreendemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;

public class SplashScreen extends Activity
{
	private static int SPLASH_TIME_OUT=5000;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
	super.onCreate(savedInstanceState);
	requestWindowFeature(Window.FEATURE_NO_TITLE);
    // hide statusbar of Android
    // could also be done later
	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
	setContentView(R.layout.splash_screen);
	
	new Handler().postDelayed(new Runnable() {
		
		@Override
		public void run() 
		{
			Intent in = new Intent(SplashScreen.this, MainActivity.class);
			startActivity(in);
			finish();
		}
	}, SPLASH_TIME_OUT);
	
	}
	
	/*
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		if(event.getAction()==MotionEvent.ACTION_DOWN)
		{
			Intent in = new Intent(SplashScreen.this, MainActivity.class);
			startActivity(in);
			finish();
		}
		return true;
	}
*/
}

