package com.example.splashscreendemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelperDentistDetail extends SQLiteOpenHelper
{
	public DbHelperDentistDetail(Context context) 
	{
		super(context, DATABASE_NAME , null,DATABASE_VERSION);
	}

	public static final String KEY_NAME="name";
	public static final String KEY_EMAIL="email";
	public static final String KEY_PROFESSION="profession"; 
	public static final String KEY_DENTAL_PRACTICE="denpractice";
	public static final String KEY_COUNTRY="country";
	public static final String KEY_WEB_DENTIST="webdentist";
	public static final String KEY_ID ="_id";
	
	public static final String DATABASE_NAME="Pepsodent";
	public static final String TABLE_NAME="dentalDetail";
	public static final int DATABASE_VERSION=2;
	
	@Override
	public void onCreate(SQLiteDatabase db) 
	{
	
		String CREATE_TABLE= "CREATE TABLE "+TABLE_NAME+ "( "+KEY_ID+ " integer, "+KEY_NAME+" text, "+KEY_EMAIL+ " text , "+KEY_PROFESSION+ " text, " +
				""+KEY_DENTAL_PRACTICE+" text, "+KEY_COUNTRY+" text, "+KEY_WEB_DENTIST+" text )";
		
		db.execSQL(CREATE_TABLE);
		
		Log.e("Create table", " "+CREATE_TABLE);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
		onCreate(db);
	}
	
	
/*	//Adding new detail
	
	public void addDentalDetail(DentalDetail dentalDetail)
	{
		SQLiteDatabase database= this.getWritableDatabase();
		
		ContentValues cv = new ContentValues();
		//cv.put(KEY_ID, dentalDetail.getId());

		cv.put(KEY_NAME, dentalDetail.getName());

		cv.put(KEY_EMAIL, dentalDetail.getEmail());

		cv.put(KEY_PROFESSION, dentalDetail.getProfession());
		
		cv.put(KEY_COUNTRY, dentalDetail.getCountry());
		
		cv.put(KEY_WEB_DENTIST, dentalDetail.getWebDentist());

		cv.put(KEY_DENTAL_PRACTICE, dentalDetail.getDentalPractice());
		
		database.insert(TABLE_NAME,null,cv);
		database.close();
		
	}
	*/
	
	
	
	
	

}
